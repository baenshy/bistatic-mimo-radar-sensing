function [G, g] = computeGain(n, v, r, f, antennaCharacterization)
% Computes the gain of an antenna with a normal vector n and polarization
% vector v (both defining the plane where the antenna is located and its
% rotation within the plane) in the direction of a vector r. 
% All 3 vectors n, v, and r can be given in the same, global Cartesian
% coordinate system. The vectors are rotated using rotation matrices
% (generated via quaternions) into the local antenna coordinate system,
% where the antenna gain pattern is applied to compute the
% direction-dependent antenna gain.
% The returned gain is a complex-valued vector of gains versus frequencies
% given in the frequency vector f.
% 
% Input Parameters:
% n             ... (3 x 1) normal vector of the current antenna (aperture)
% v             ... (3 x 1) direction of the polarization vetor in global coordinates
% r             ... (3 x 1) vectorial distance from the current antenna (of which the
%                   gain is to be computed) to the respective other antenna.
%                   E.g.:   the current antenna is located at p1,
%                           the antenna we are looking to is located at p2
%                           the vectorial distance is r = p2 - p1
% f             ... (Nf x 1) input (vector/scalar) of Nf frequencies at  
%                   which the gain is to be evaluated.
%
% Output Parameters:
% G             ... (Nf x 1) gain in the direction of r
%
%
% Note: Polarization losses omitted in this version.
%
% Authors:
% Benjamin Deutschmann and Max Graber, July 2022.

  gainFunction = antennaCharacterization.Gain;
  phi_ff = antennaCharacterization.Phi;
  theta_ff = antennaCharacterization.Theta;
  freq = antennaCharacterization.Frequency;

  % Unit vecotrs in local antenna coordinate system (of the antenna gain pattern)
  ex = [1;0;0]; ey = [0;1;0]; ez = [0;0;1];
  origin = [0;0;0];

  % Ensure unit mangitude:
  n = n/norm(n);
  v = v/norm(v);

  % Transformation 1: rotate n towards ez (antenna orientation in anechoic chamber)
  % Compute a vector orthogonal to the nz-plane:
  u = cross(n,ez);  u = u/norm(u);
  % Compute angle between vectors to be rotated:
  theta = acos(n.' * ez);
  % Compute rotation matrix using a quaternion:
  q1 = quaternion(cos(theta/2),sin(theta/2)*u(1),sin(theta/2)*u(2),sin(theta/2)*u(3));

  R1 = quat2rotm(q1.');   % first rotation matrix
  vt = R1*v;              % also rotate polarization vector for later alignment with local coordinate system


  % Transformation 2: shift v towards ex
  % Now we aim to rotate around the ez axis:
  u = ez; u = u/norm(u);
  % Compute angle between vectors to be rotated: (local, measured polarization assumed to be ex)
  theta = acos(vt.' * ex);
  % Compute rotation matrix using a quaternion:
  q2 = quaternion(cos(theta/2),sin(theta/2)*u(1),sin(theta/2)*u(2),sin(theta/2)*u(3));
  R2 = quat2rotm(q2.');   % second rotation matrix

  % Complete rotation matrix from global coordinates to local antenna
  % coordinates (multiply with vector in global coordinates from the left):
  R = R2*R1;      

  % Sanity check: the complete transformation would tilt the polarization to ex and n towards ez
  vt2 = R*v;    
  assert(abs(vt2' * ex - 1) < 1e-5, 'Polarization rotation failed.')
  nt2 = R*n;
  assert(abs(nt2' * ez - 1) < 1e-5, 'Antenna normal rotation failed.')

  er = r/norm(r);   % unit look direction in global Cartesian coordinates
  ert = R*er;       % unit look direction transformed to local Cartesian coordinates

  [~, gainInputAzimuth, gainInputElevation] = rangeAzimuthElevation(ert);
  if(gainInputAzimuth < 0)
      gainInputAzimuth = gainInputAzimuth + 2*pi;
  end

  [Xq,Yq,Zq] = meshgrid(gainInputAzimuth:0.5:gainInputAzimuth,gainInputElevation:0.1:gainInputElevation, f);

  [val,foundPhiIndex]=min(abs(phi_ff-gainInputAzimuth));
  [val,foundThetaIndex]=min(abs(theta_ff-gainInputElevation));
  [val,foundFrequencyIndex]=min(abs(freq-f));

  % Take closest value (fast) instead of interpolating (slow):
  G = gainFunction(foundThetaIndex,foundPhiIndex,foundFrequencyIndex);
end

