function [range, azimuth, elevation] = rangeAzimuthElevation(inputVector)
  range = norm(inputVector);
  elevation = acos(inputVector(3)/range);
  azimuth = atan2(inputVector(2),inputVector(1));
end

