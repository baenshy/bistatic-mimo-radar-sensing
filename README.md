# Bistatic MIMO Radar Sensing

This repository includes the Matlab code and synthetic aperture measurement data accompanying the publication "Bistatic MIMO Radar Sensing of Specularly Reflecting Surfaces for Wireless Power Transfer" [1].

![A three-dimensional model of the measurement scenario](scenario.png)

## Detailed Description
The measurement data <b>VNA_20220722_232002_XETS_reduced.mat</b> includes a data matrix $\mathbf{R}$ acquired with a synthetic aperture measurement testbed described in [2] and [3]. 
Measured were $N_f=1000$ frequency steps in a band from $3-10$GHz of the scattering parameter $S_{21}$ between a synthetic $51$-ULA with antenna positions saved in the file <b>ULA.mat</b> and a synthetic $(13\times 13)$-URA with antenna positions saved in the file <b>URA.mat</b>.
The file <b>XETSantennaCharacterization.mat</b> holds antenna gains of an XETS antenna [4] characterized in an anechoic chamber. XETS antennas were used on both the ULA (oriented towards the negative $x$-direction) and on the URA (oriented towards the positive $x$-direction).<br>
These data have been used to perform ultra-wideband (UWB) bistatic radar imaging and wireless power transfer (WPT).
Our implementation is provided in <b>MAIN_wall_detection.mat</b>. 
We invite readers to have a look at the tasks we have solved with the given dataset in [1] and encourage them to implement their own methods.

## Instructions: Using the Dataset
To use the dataset, it is sufficient to load the respective data files. It is <b>not</b> necessary to run any scripts, however, the first sections in <b>MAIN_wall_detection.mat</b> may provide some guidance on how the data may be used.<br>
Load the .mat-files in Matlab using the load(\<filename\>) command. Each file contains data that can be used to perform bistatic MIMO radar imaging:

- <b>VNA_20220722_232002_XETS_reduced.mat</b>: Contains a $(Nf \times M \times N)$ matrix $\mathbf{R}$ holding $N_f$ MIMO channel matrices of dimensions $(M \times N)$ and an $N_f$-vector _frequencies_ holding the frequency-domain steps in the band $3-10$GHz.
- <b>ULA.mat</b>: Contains a struct _ULA_ holding the center of gravity position $\mathbf{p}$ of the ULA in global coordinates and a $(3 \times M)$ matrix $\mathbf{p}_m$ of antenna positions relative to $\mathbf{p}$.
- <b>URA.mat</b>: Contains a struct _URA_ with $N$-vectors $\mathbf{x}_v$, $\mathbf{y}_v$, and $\mathbf{z}_v$, holding the positions of the ULA antennas in global coordinates.
- <b>XETSantennaCharacterization.mat</b>: Contains a struct _antennaCharacterization_ holding the measured _Gain_ of an XETS antenna [4] as a function of spherical coordinates _Phi_ and _Theta_, and the _Frequency_. Note that the characterization is valid only for the ULA-side, where an absorber is mounted on the "backside" of the antenna which effectively removes the lobe facing the wall. Polarization vectors were aligned with the $z$-axis. Measurement reference planes are located at the antenna ports after calibration - the antennas can be de-embedded by accounting for an electrical length of approx. $40$cm (i.e., $20$cm on both the TX and RX side).

---
## References
(Unpublished work will appear in the <a href="https://reindeer-project.eu/results-downloads/">REINDEER Project Publications</a>.)<br>
[1] B. J.B. Deutschmann, M. Graber, T. Wilding, K. Witrisal, "Bistatic MIMO Radar Sensing of Specularly Reflecting Surfaces for Wireless Power Transfer", 2023 <a href="https://arxiv.org/abs/2305.05002">DOI: 10.48550/ARXIV.2305.05002</a>. <br>
[2] REINDEER Project, “Propagation characteristics and channel models for RadioWeaves including reflectarrays,” Deliverable ICT-52-2020 / D1.2, 2023 (unpublished). <br>
[3] REINDEER Project, “System design study for energy-neutral devices interacting with the RadioWeaves infrastructure,” Deliverable ICT-52-2020 / D4.1, 2023 (unpublished). <br>
[4] J. R. Costa, C. R. Medeiros, and C. A. Fernandes, “Performance of a crossed exponentially tapered slot antenna for UWB systems,” IEEE Trans. Antennas Propag., vol. 57, no. 5, pp. 1345–1352, May 2009.

---
[![CC BY 4.0][cc-by-shield]][cc-by]

This work is licensed under a
[Creative Commons Attribution 4.0 International License][cc-by].

[![CC BY 4.0][cc-by-image]][cc-by]

[cc-by]: http://creativecommons.org/licenses/by/4.0/
[cc-by-image]: https://i.creativecommons.org/l/by/4.0/88x31.png
[cc-by-shield]: https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg
