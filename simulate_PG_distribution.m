%% WPT: Simulated Path Gain
% Show what path gain PG is expected in the inferred environment, based
% entirely on simulated channels.
%
% Input:    estimated locations of ULA mirror sources, 
%           RX antenna position (at the URA center),
%           previously computed weights
% Output:   entirely simulated path gain distribution across the grid for
%           given weights

useAntennaCharacterization = true;    % use (measured) antenna gain patterns (slow)
antennaType = "xets";
p_UE = [URA.xv(iWPTantenna); URA.yv(iWPTantenna); URA.zv(iWPTantenna) ];

h_model = 0;

Hnk = zeros(M,K,N);    % simulated channels on the grid
yn = zeros([13 13]);

%% Simulation

fprintf('Simulating %d points: \n',N);
for nn = 1:N
  fprintf('%d ',nn);
  pn = [URA.xv(nn), URA.yv(nn), URA.zv(nn)].';
  for k = 1:K   % iterate over all K image sources (includind the LoS)
    rag = repmat(pn,[1,M]) - RW{k}.pm_global; % (3 x 51) vectorial distances of kth mirror source to the UE
    dag = sqrt(sum(rag.^2,1));                  % scalar distances from the array to the UE

    gainVector = ones(size(dag));
    if(useAntennaCharacterization)  % assemble the gain vector for M antennas of mirror source kk and grid point pg
      m = 1;
      for lookVector = rag          % iterate over all antennas
          % TX: mirrored around
          n_VA = RW{k}.H * [-1; 0; 0];  % normal vector of TX antenna mirrored via Householder matrix
          antennaGainTX = computeGain(n_VA, [0; 0; 1], lookVector , fc, antennaCharacterization);
          % RX: staying static, BUT we do NOT have its measured gain pattern on the rear side 
          % (in the correct form at least). Workaround: make it symmetric:
          if (dot([1; 0; 0], lookVector) < 1) % front facing
            antennaGainRX = computeGain([1; 0; 0], [0; 0; 1], -lookVector, fc, antennaCharacterization);
          else % rear facing
            antennaGainRX = computeGain([-1; 0; 0], [0; 0; 1], -lookVector, fc, antennaCharacterization);
          end
      gainVector(m) = sqrt(antennaGainTX) * sqrt(antennaGainRX);
      m = m + 1;    % increased for every iteration
      end
    end

    h_model = (gainVector .* exp(-2i*pi/lambda * dag)*lambda./(4*pi*dag)).'; % geometry-based channel prediction
    Hnk(:,k,nn) = h_model; 
    yn(nn) = yn(nn) + w.' * h_model;
  end
end
fprintf('\nSimulation completed!\n');
Hn = squeeze( sum(Hnk,2) );


%% Plotting

% Wave field interpolation for plotting:
xx = linspace(URA.xv(1), URA.xv(end), 13);
yy = linspace(URA.yv(1), URA.yv(end), 13);
xq = linspace(URA.xv(1), URA.xv(end), INTERP_RES);
yq = linspace(URA.yv(1), URA.yv(end), INTERP_RES);
Re_y = real(yn);
Im_y = imag(yn);
interpRe = interp2(xx,yy.', Re_y, xq, yq.', 'spline');
interpIm = interp2(xx,yy.', Im_y, xq, yq.', 'spline');
y_interp = interpRe + 1j*interpIm;

f9 = figure(9);   %  ---------- ---------- ---------- ---------- ----------
f9.Position = [0 500 500 500];
imagesc(xq, yq, 10*log10(abs(y_interp.').^2)); hold on;
set(gca,'YDir','normal'); set(gca,'XDir','normal');
plot(URA.xv(iWPTantenna), URA.yv(iWPTantenna), 'bo');
PG_max_sim = 10*log10(abs(yn(iWPTantenna))^2);
text(URA.xv(iWPTantenna)+1e-2, URA.yv(iWPTantenna)+1e-2,sprintf('%2.1fdB',PG_max_sim),'FontSize',PLOT_FS);

view(90,90)
axis equal
colormap(cmap_interp);
hb = colorbar;
hb.FontSize = PLOT_FS;
hb.LineWidth = PLOT_LW;
colormap(cmap_interp);
xlabel('x in m'), ylabel('y in m')
ylabel(hb,'PG in dB')
set(gca,'FontSize',PLOT_FS,'LineWidth',PLOT_LW);
xlim([URA.xv(end) URA.xv(1)]); ylim([URA.yv(1) URA.yv(end)]);
hold off
%caxis([PG_max_sim-30 PG_max_sim])
caxis([cmax_WPT-30 cmax_WPT])

if SHOW_FIGURE_TITLES
  title(['Simulated PG']);
end

fprintf('Done.\n');
fprintf('---------- ---------- ---------- ---------- ---------- ---------- ---------- \n');
