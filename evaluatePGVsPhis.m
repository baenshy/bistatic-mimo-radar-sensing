function PG_dB = evaluatePGVsPhis(phis, h_meas, H_model)
    % H_model   ... (L x K) simulated channel vectors
    % phis      ... (K x 1) phase shifts
    new_h_model = H_model * exp(1j*phis);   % (L x 1) adapted channel vector
    w = conj(new_h_model)/norm(new_h_model);
    alpha = w.' * h_meas;
    PG_dB = 10*log10(abs(squeeze(alpha)).^2); 
end

