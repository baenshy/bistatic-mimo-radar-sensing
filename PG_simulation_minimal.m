% Description: minimal version of the PG simulation as in [1]
%
% [1] Location-based Initial Access for Wireless Power Transfer with
% Physically Large Arrays, https://arxiv.org/pdf/2202.10749.pdf
% [2] XL-MIMO Channel Modeling and Prediction for
% Wireless Power Transfer, https://arxiv.org/pdf/2302.11969.pdf
% 

%% Specify array

f = 3.8e9;        % choice of antenna frequency
c = 3e8;
lambda = c/f;
D = lambda/2;         % assume some physical antenna aperture
d_F = 2*D^2/lambda;   % Fraunhofer distance

% Choice of array layout (choose ULA):
spacing = lambda/2;         % array spacing
L = 100;                  % number of antennas

% Assemble array:
array.xv = (1:L) * spacing;           % x-positions (uncentered)
array.xv = array.xv - mean(array.xv); % x-positions (centered)
array.yv = zeros(1,L);                % y-positions 
array.zv = zeros(1,L);                % z-positions 

% Choose UE (energy neutral device) position:
p_EN = [0,3,0]';

% Choose grid to evaluate the path gain at:
X_MIN = -5;   X_MAX = 5;      % x-limits within the floorplan
Y_MIN = d_F;  Y_MAX = 5;      % y-limits within the floorplan
dx = 0.04;                % spatial resolution of radar image in x-direction
dy = 0.04;                % spatial resolution of radar image in y-direction

%% Compute true channel and optimal weights

P_array = [array.xv; array.yv; array.zv];   % (3 x L) matrix capturing the antenna layout, see [2, page 3]

r_EN = repmat(p_EN,[1 L]) - P_array;        % (3 x L) vectorial distances from array to device, see [2, page 3]
d_EN = sqrt(sum(r_EN.^2,1))';               % (L x 1) scalar distances from array to device

% Evaluate (2); i.e., the true channel vector to the device:
h = lambda/sqrt(4*pi) * 1./(sqrt(4*pi) * d_EN) .* exp(-1i * 2 * pi / lambda * d_EN);

% Evaluate (9); i.e., compute MRT weights:
w = conj(h)/norm(h);

%% Compute path gain
% Generate meshgrid of positions for the 2D window:
xv_ = X_MIN:dx:X_MAX;     Nx = length(xv_);
yv_ = Y_MIN:dy:Y_MAX;     Ny = length(yv_);
[x_mesh,y_mesh,z_mesh] = meshgrid(xv_,yv_,array.zv(1));  
Ng = numel(x_mesh);       % number of grid points
assert(Ng == Nx*Ny,'No valid grid.');
fprintf('Evaluating %d points: \n',Ng);

y = deal(zeros(size(x_mesh)));        % amplitudes in (1)
i_loop = 0;                           % loop iteration
for gg = 1:Ng
  fprintf('%d \n',gg);
  pg = [x_mesh(gg), y_mesh(gg), z_mesh(gg)].';
  
  rag = repmat(pg,[1,L]) - P_array;         % (3 x L) vectorial distances from array to grid point
  dag = sqrt(sum(rag.^2,1))';               % (L x 1) scalar distances from array to grid point

  hg = lambda/sqrt(4*pi) * 1./(sqrt(4*pi) * dag) .* exp(-1i * 2 * pi / lambda * dag);
  y(gg) =  w.' * hg;
end
fprintf('Done.\n');

%% Compute Path Gain at EN device
  raEN = repmat(p_EN,[1,L]) - P_array;         % (3 x L) vectorial distances from array to grid point
  daEN = sqrt(sum(raEN.^2,1))';               % (L x 1) scalar distances from array to grid point

  hEN = lambda/sqrt(4*pi) * 1./(sqrt(4*pi) * daEN) .* exp(-1i * 2 * pi / lambda * daEN);
  y_EN =  w.' * hEN;
  PG_EN = 10*log10(abs(y_EN).^2);

  fprintf('Path Gain at UE is %2.2fdB\n',PG_EN);

%% Plot Scenario:
f1 = figure(1);  f1.Position = [0 500 800 500]; % prepare figure
plot(array.xv,array.yv, 'g.', 'MarkerSize', 10);
hold on
  PG_dB = 10*log10(abs(y).^2);
  imagesc(xv_, yv_, PG_dB); hold on; 
  plot(p_EN(1),p_EN(2), 'bo', 'MarkerSize', 10);
  set(gca,'YDir','normal'); set(gca,'XDir','normal');
  xlabel('x in m');
  ylabel('y in m');
  cb = colorbar; ylabel(cb,'PG in dB');
  set(gca,'FontSize',12,'LineWidth',1.5);
  caxis([max(max(PG_dB)) - 40, max(max(PG_dB))]);
  xlim([X_MIN X_MAX]); ylim([0 Y_MAX]);
hold off