% Description:
% Code accompanying the paper entitled
% "Bistatic MIMO Radar Sensing of Specularly Reflecting Surfaces for 
% Wireless Power Transfer".
%
% Authors: Benjamin J.B. Deutschmann, Max Graber, Thomas Wilding
%
% Date: 12.12.2022
%

%% USER PARAMETERS:
USE_LOW_RES         = true;   % runs faster for testing
LOAD_HIGH_RES       = true; 	% loads high res imaging data rather than computing it
USE_DUAL_BAND_ONLY  = false;  % loads high res imaging data rather than computing it
INTERP_RES          = 500;    % interpolation resolution for PG distributions
SHOW_FIGURE_TITLES  = true;   % show titles in figures

% Measurement settings:
iWPTantenna   = 85;           % the antenna in the URA center is chosen for WPT
iWPTfrequency = 114;          % a center frequency of 3.7918GHz is chosen for WPT

%% Load Measurement Data
fprintf('---------- ---------- ---------- ---------- ---------- ---------- ---------- \n');
fprintf('Loading data.\n');

% PLOT SETTINGS: ---------- ----------
PLOT_LW = 2;    % plot line width 
PLOT_Lthin = 1.2;    % plot line width 
PLOT_FS = 20;   % plot font size
PLOT_MS = 5;    % plot marker size
% ---------- ---------- ----------
% REINDEER styleguide colors:
RD.lightgreen =   [141 192 69]/255;
RD.maroon =       [133 56 90]/255;
% ---------- ---------- ----------

load('VNA_20220722_232002_XETS_reduced.mat');   % load measured VNA data 'R' and 'frequencies'
load('XETSantennaCharacterization.mat')         % load XETS antenna gain pattern
load('ULA.mat');                                % load ULA positions (struct 'ULA')
load('URA.mat');                                % load URA positions (struct 'ULA')
load("cmap_interp.mat");                        % a custom colormap
cmax_WPT = -27.5;                               % upper PG colorbar limit for WPT

fc = frequencies(iWPTfrequency);    % carrier frequency for WPT
c = physconst('Lightspeed');
lambda = c/fc;                      % wavelength

%% Sensing of Surfaces:   A. Radar Imaging 
% Input:      VNA measurement data R (channel matrix)
%             array positions 'ULA' and 'URA'
% Output:     Radar image

fprintf('Starting bistatic sensing procedure.\n');

M = size(R,2);    % ULA: M= 51 RX antennas
N = size(R,3);    % URA: N=169 TX antennas

% [Sensing] Specification of the location and extent of 2D radar image:
pz = 1.2850;              % height of the 2D window vertically colocated w. the ULA
X_MIN = -4; X_MAX = -1;   % x-limits within the floorplan
Y_MIN = 3;  Y_MAX = 7;    % y-limits within the floorplan
dx = 0.01;                % spatial resolution of radar image in x-direction
dy = 0.01;                % spatial resolution of radar image in y-direction
df = 1;                   % integer step within frequency vector (df = 1 --> full BW)

if (USE_LOW_RES)          % overwrite with low-res settings for a faster run
  warning('LOW RESOLUTION option selected for faster testing!')
  dx = 0.10;              % spatial resolution of radar image in x-direction
  dy = 0.10;              % spatial resolution of radar image in y-direction
  df = 10;                % use only every 10-th frequency bin measured
end

% Generate meshgrid of positions for the 2D window:
xv_ = X_MIN:dx:X_MAX;     Nx = length(xv_);
yv_ = Y_MIN:dy:Y_MAX;     Ny = length(yv_);
[x_mesh,y_mesh,z_mesh] = meshgrid(xv_,yv_,pz);  
Ng = numel(x_mesh);       % number of grid points
assert(Ng == Nx*Ny,'No valid grid.');

alphas = deal(zeros(size(x_mesh)));   % amplitudes corresponding to the "reflectivity map I(p)"
i_loop = 0;                           % loop iteration


if USE_DUAL_BAND_ONLY
  % "Exploiting the new Wi-Fi 6E band available in Europe / USA+":
  % [1] https://ieeexplore.ieee.org/document/9994727 (DOI: 10.1109/ACCESS.2022.3231229)
  % [2] https://www.derstandard.de/story/2000140732610/wi-fi-6e-kommt-mitte-jaenner-oesterreich-macht-endlich-den
  f_min = 5.945e9;      i_fmin = find(frequencies > f_min,1);
  f_max_EU = 6.425e9;   i_fmax_EU = find(frequencies > f_max_EU,1);
  f_max_US = 7.125e9;   i_fmax_US = find(frequencies > f_max_US,1);
  
  % Please select your desired frequency band here:
  %f_vec = i_fmin:1:i_fmax_EU; fprintf('Dual-band: EU band chosen!\n');  % EU band
  f_vec = i_fmin:1:i_fmax_US; fprintf('Dual-band: US band chosen!\n');  % US band
  
  % Dual band with 100MHz at 3.8GHz:
  f_min_3G8 = 3.8e9 - 50e6;  i_fmin_3G8 = find(frequencies > f_min_3G8,1);
  f_max_3G8 = 3.8e9 + 50e6;  i_fmax_3G8 = find(frequencies > f_max_3G8,1);
  f_vec = [i_fmin_3G8:1:i_fmax_3G8 f_vec];
else
  f_vec = 1:df:length(frequencies); fprintf('Full frequency band used!\n');
end

f1 = figure(1);  f1.Position = [0 500 800 500]; % prepare figure for live updates
for iif = f_vec
  Nf = length(f_vec);           % number of frequency steps to evaluate
  i_loop = i_loop + 1;          % current loop iteration i_loop in {1 ... Nf}
  f_curr = frequencies(iif);    % frequency at current iteration
  lambda_curr = c/f_curr;       % wavelength at current iteration
  
  for gg = 1:Ng
    px = x_mesh(gg);
    py = y_mesh(gg);
    pz = z_mesh(gg);
    
    % ---------- WAITBAR ----------
    if ((gg == 1)&&(i_loop==1))
        h_wait = waitbar(0,'Computing remaining time...');
        tic;
        gg_overall = Nf*Ng;
    end
    if (mod(gg,10) == 0)
        delta_T = toc;
        gg_completed = (i_loop-1)*Ng + gg;
        gg_remaining = gg_overall - gg_completed;
        T_remaining = delta_T/gg_completed * gg_remaining;
        progress = gg_completed / gg_overall;
        waitbar(progress,h_wait,['Frequency point ' num2str(i_loop) ' of ' num2str(Nf) ... 
          '. T (MM:SS) remaining: ' num2str(T_remaining/60,'%.0f') ':' num2str(mod(T_remaining,60),'%.0f')]);
    end
    % ---------- WAITBAR ----------
    
    % Compute geometry-based channel predictions: -------------------------
    pg = [px;py;pz];    % assemble current position within the 2D radar image meshgrid
    
    % Geometry-based RX channel vector prediction:
    rag = repmat(ULA.p,[1,M]) + (ULA.pm) - repmat(pg,[1,M]);  % (3 x M) vectorial distances from grid to RX ULA
    dag = sqrt(sum(rag.^2,1));    % (1 x M) scalar distances from grid point pg to the RX ULA
    h_model = exp(-2i*pi/lambda_curr * dag)*lambda_curr./(4*pi*dag); % geometry-based channel prediction
    h_model_RX = h_model.';  % (M x 1) modeled channels from TX ULA to grid point pg
    
    % Geometry-based TX channel vector prediction:
    p_TX = [URA.xv(:).'; URA.yv(:).'; URA.zv(:).'];  % (3 x N) matrix of TX URA positions
    rag_TX = repmat(pg,[1,N]) - p_TX;  % (3 x N) vectorial distances from TX antennas to grid point pg
    dag_TX = sqrt(sum(rag_TX.^2,1));   % (1 x N) scalar distances from TX URA to grid point pg
    h_model_TX = exp(-2i*pi/lambda_curr * dag_TX)*lambda_curr./(4*pi*dag_TX); % geometry-based channel prediction
    h_model_TX = h_model_TX.';  % (N x 1) modeled channels from TX URA to grid point pg
    
    % Compute beamforming weights: ----------------------------------------
    w_TX = conj(h_model_TX)/norm(h_model_TX);   
    w_RX = conj(h_model_RX)/norm(h_model_RX);   
    
    % We need to account for the electrical antenna length (measured)
    % because the VNA reference planes after TOSM calibration are before
    % the antennas:
    ELECTRICAL_ANTENNA_LENGTH = 0.4;
    XETS_CORRECTION = exp(-2i*pi*f_curr/c*(-ELECTRICAL_ANTENNA_LENGTH));  % deembed the length of the antenna
    
    alpha_p = w_RX.' * squeeze(R(iif,:,:)) * w_TX .* XETS_CORRECTION;     % sensing amplitude ("reflectivity")
    alphas(gg) = alphas(gg) + alpha_p;
  end
    % Dynamic plot updates:  ---------- ---------- ---------- ---------- --
    Pg = 10*log10(abs(alphas).^2);
    if (i_loop == 1)
      caxis_dB_range = 60; 
      h_img = imagesc(xv_,yv_,Pg);
      view(-90,-90)
      hb = colorbar;
      hb.FontSize = PLOT_FS;
      hb.LineWidth = PLOT_LW;
      colormap(cmap_interp);
      xlabel('x in m'), ylabel('y in m')
      ylabel(hb,'|I|^2 in dB')
      axis equal
      set(gca,'FontSize',PLOT_FS,'LineWidth',PLOT_LW);
      ylim([min(yv_) max(yv_)]);
      xlim([min(xv_) max(xv_)]);
    else
      h_img.CData = Pg;
      c_max = max(max(Pg))-3;
      caxis([c_max-caxis_dB_range c_max])
      if SHOW_FIGURE_TITLES
        title(sprintf('Sensing with %2.1f%% of the chosen bandwidth',i_loop/Nf * 100));
      end
    end %  ---------- ---------- ---------- ---------- ---------- ---------
end
close(h_wait);    % close waitbar

%% Sensing of Surfaces:   B. Edge Detection and Surface Estimation
% Part I) estimation of wall positions
%
% Input:    sensing data (radar image)
% Output:   inferred wall positions

if LOAD_HIGH_RES      % to save time, high-res sensing data can be loaded 
  warning('Using precomputed radar imaging data.');
  if (USE_DUAL_BAND_ONLY == false)
    load('alphas_highres.mat');       % using the complete 3-9GHz band imaging data
  else
    load('alphas_DUAL_US.mat');       % using the the dual-band (US) imaging data
  end
end

if (USE_DUAL_BAND_ONLY == false)  % using the complete 3-9GHz band imaging data
  % ---------- CUSTOM SETTINGS ----------
  IMG_DATA = 10*log10(abs(alphas).^2);     % working in the log power domain
  TH_EDGE_DETECTION = 0.7;  	% the threshold for the edge detection (hand-picked)
  %TH_EDGE_DETECTION = 0.1;  	% a possible setting for the dual-band scenario (hand-picked)
  RHO_HOUGH_RES = 0.1;        % (IMPORTANT!) range resolution of hough transform (hand-picked)
  THETA_HOUGH_RES = 0.1;      % (IMPORTANT!) angle resolution of hough transform (hand-picked)
  TH_HOUGH_PEAKS = 0.05;      % the threshold for the hough transform peak detection (hand-picked)
  NO_HOUGH_PEAKS = 5;         % the number of peaks for houghpeaks() (hand-picked)
  GAP_HOUGH_LINES = 26;       % the allowed gap filling distance (hand-picked)
  LEN_HOUGH_LINES = 60;       % the minimum length of the hough lines (hand-picked)
  % ---------- CUSTOM SETTINGS ----------
else                              % using the the dual-band (US) imaging data
  % ---------- CUSTOM SETTINGS ----------
  IMG_DATA = 10*log10(abs(alphas).^2);     % working in the log power domain
  TH_EDGE_DETECTION = 0.85;  	% the threshold for the edge detection (hand-picked)
  RHO_HOUGH_RES = 0.1;        % (IMPORTANT!) range resolution of hough transform (hand-picked)
  THETA_HOUGH_RES = 0.1;      % (IMPORTANT!) angle resolution of hough transform (hand-picked)
  TH_HOUGH_PEAKS = 0.05;      % the threshold for the hough transform peak detection (hand-picked)
  NO_HOUGH_PEAKS = 5;         % the number of peaks for houghpeaks() (hand-picked)
  GAP_HOUGH_LINES = 10;       % the allowed gap filling distance (hand-picked)
  LEN_HOUGH_LINES = 40;       % the minimum length of the hough lines (hand-picked)
% ---------- CUSTOM SETTINGS ----------
end

BW = edge(IMG_DATA,'canny',TH_EDGE_DETECTION);  % perform edge detection
[H,T,Rh] = hough(BW,'RhoResolution',RHO_HOUGH_RES,'ThetaResolution',THETA_HOUGH_RES);
P  = houghpeaks(H,NO_HOUGH_PEAKS,'threshold',ceil(TH_HOUGH_PEAKS*max(H(:)))); % find peaks
lines = houghlines(BW,T,Rh,P,'FillGap',GAP_HOUGH_LINES,'MinLength',LEN_HOUGH_LINES); % find lines of reflections

% Transformation to the global room coordinate system:
scale   = (max(xv_) - min(xv_))/size(BW,2);
offset  = [min(xv_) min(yv_)];

K = length(lines) + 1;
fprintf('The number of detected walls is %d\n',K-1);

f2 = figure(2); %  ---------- ---------- ---------- ---------- ---------- -
f2.Position = [800 500 500 500];
imagesc(xv_,yv_,(IMG_DATA)); hold on;
max_len = 0;
lines_tf = lines;
for k = 1:K-1
  lines_tf(k).point1 = scale * lines(k).point1 + offset;  % transform to room coordinates
  lines_tf(k).point2 = scale * lines(k).point2 + offset;  % transform to room coordinates
   xy = [lines_tf(k).point1; lines_tf(k).point2];
   plot(xy(:,1),xy(:,2),'LineWidth',PLOT_LW,'Color',RD.maroon);     % plot detected lines
   plot(xy(1,1),xy(1,2),'o','LineWidth',PLOT_LW,'Color',RD.maroon); % plot line ends
   plot(xy(2,1),xy(2,2),'o','LineWidth',PLOT_LW,'Color',RD.maroon); % plot line ends
end
plot(URA.xv,URA.yv,'.','MarkerSize',1,'Color',RD.lightgreen);        % grid positions
view(-90,-90)
colormap(cmap_interp);
xlabel('x in m'), ylabel('y in m');
set(gca,'FontSize',PLOT_FS,'LineWidth',PLOT_LW);
axis equal

if SHOW_FIGURE_TITLES
  title(sprintf('Hough-TF: %d lines detected',K-1));
end

cmax = max(max(IMG_DATA));
ylim([min(yv_) max(yv_)]);
xlim([min(xv_) max(xv_)]);
caxis([cmax-50 cmax]);
%  ---------- ---------- ---------- ---------- ---------- ---------- ------

%% Sensing of Surfaces:   B. Edge Detection and Surface Estimation
% Part II) computation of mirror sources
%
% Input:    inferred wall positions
% Output:   estimated locations of mirror sources

RW = {};          % initialize empty RadioWeave (RW) arrays
RW{1} = ULA;    % add LoS source
RW{1}.pm_global = RW{1}.p + RW{1}.pm;
RW{1}.H = eye(3); % Householder matrix of LoS component

f3 = figure(3);   %  ---------- ---------- ---------- ---------- ----------
f3.Position = [1300 500 500 500];
imagesc(xv_,yv_,IMG_DATA); hold on;
plot(RW{1}.p(1)+RW{1}.pm(1,:),RW{1}.p(2)+RW{1}.pm(2,:),'.','Color',RD.lightgreen);

for k = 1:K-1
  % i) compute wall normal vector
  r = lines_tf(k).point2 - lines_tf(k).point1;  % vectorial distance in 2D
  nw = [r(2) -r(1) 0]'/norm(r);                 % normal vector in 3D (assuming the same plane..)
  % ii) compute Householder matrix
  RW{k+1}.H = eye(3) - 2 * nw*nw.';
  % iii) compute array layout mirrored around its center of gravity
  RW{k+1}.pm = RW{k+1}.H * ULA.pm;
  % iv) apply shift to correct position
  point1_3D = [lines_tf(k).point1 ULA.p(3)]';
  dw = (ULA.p-point1_3D).' * nw;
  RW{k+1}.p_global = ULA.p - 2*dw*nw;
  RW{k+1}.pm_global = RW{k+1}.p_global + RW{k+1}.pm;
  plot(RW{k+1}.pm_global(1,:),RW{k+1}.pm_global(2,:),'.','Color',RD.lightgreen);
end

plot(URA.xv,URA.yv,'.','MarkerSize',1,'Color',RD.lightgreen);        % grid positions
view(-90,-90)
colormap(cmap_interp);
xlabel('x in m'), ylabel('y in m');
set(gca,'FontSize',PLOT_FS,'LineWidth',PLOT_LW);
axis equal
xlim([-5.75 0]); ylim([3.3 9]);
caxis([cmax-50 cmax]);

if SHOW_FIGURE_TITLES
  title(sprintf('Locations of %d (mirror) sources',K));
end
%  ---------- ---------- ---------- ---------- ---------- ---------- ------


%% WPT: A. Geometry-based Beamforming
% Part II) computation of mirror sources
%
% Input:    estimated locations of ULA mirror sources, 
%           RX antenna position (at the URA center)
% Output:   predicted channel vectors hk for each mirror source k in {1 ... K}

useAntennaCharacterization = true;    % use (measured) antenna gain patterns (slow)
antennaType = "xets";
p_UE = [URA.xv(iWPTantenna); URA.yv(iWPTantenna); URA.zv(iWPTantenna) ];
Hk = zeros(M,K);  % initialize (M x K) matrix holding K SMC channel vectors

for k = 1:K   % iterate over all K image sources (includind the LoS)
  rag = repmat(p_UE,[1,M]) - RW{k}.pm_global; % (3 x 51) vectorial distances of kth mirror source to the UE
  dag = sqrt(sum(rag.^2,1));                  % scalar distances from the array to the UE

  gainVector = ones(size(dag));
  if(useAntennaCharacterization)  % assemble the gain vector for M antennas of mirror source kk and grid point pg
    m = 1;
    for lookVector = rag          % iterate over all antennas
        % TX: mirrored around
        n_VA = RW{k}.H * [-1; 0; 0];  % normal vector of TX antenna mirrored via Householder matrix
        antennaGainTX = computeGain(n_VA, [0; 0; 1], lookVector , fc, antennaCharacterization);
        % RX: staying static, BUT we do NOT have its measured gain pattern on the rear side 
        % (in the correct form at least). Workaround: make it symmetric:
        if (dot([1; 0; 0], lookVector) < 1) % front facing
          antennaGainRX = computeGain([1; 0; 0], [0; 0; 1], -lookVector, fc, antennaCharacterization);
        else % rear facing
          antennaGainRX = computeGain([-1; 0; 0], [0; 0; 1], -lookVector, fc, antennaCharacterization);
        end
    gainVector(m) = sqrt(antennaGainTX) * sqrt(antennaGainRX);
    m = m + 1;    % increased for every iteration
    end
  end

  h_model = gainVector .* exp(-2i*pi/lambda * dag)*lambda./(4*pi*dag); % geometry-based channel prediction
  Hk(:,k) = h_model; 
end


%% WPT: B. Optimization of Beam-phases
% Part I) computation of the PG distribution accross the URA
%
% Input:  predicted channel vectors hk for each mirror source k in {1 ... K}
%         measured channel vectors
% Output: phase optimized BF weights to the UE 
%         power (path gain) distribution accross the URA

CONSTANT_PLOTTING_PHASE_SHIFT = deg2rad(-90);   % for visualization; without loss of generality
h_true = R(iWPTfrequency,:,iWPTantenna).';      % extract the "true" (measured; assumed unknown) channel vector

% i) Optimization of MPC beam phases:
options = optimset('Display','off');    % omit output notifications for fminsearch
fun = @(phis)-1*evaluatePGVsPhis([0; phis], h_true, Hk);  % first phase kept at 0 degrees
[phis_opt, PL_dB_val] = fminsearch(fun, 2*pi*rand(K-1,1),options);        % try random initializations
phis = [0; phis_opt];

% ii) Compute the "phase-optimized channel vector"
h_opt = Hk * exp(1j*phis);

% iii) Compute optimized / non-optimized phasors alpha_k
alphas_non_opt = {};  % non-optimized phasors at the chosen RX antenna 'iWPTantenna'
alphas_opt = {};      % optimized predicted phasors at the chosen RX antenna 'iWPTantenna'
for k = 1 : K
    % Non-ptimized amplitudes:
    w = conj( Hk(:,k) ) / norm(sum(Hk,2));
    alphas_non_opt{end + 1} = w.'*h_true;
    
    % Optimized amplitudes:
    w = conj( Hk(:,k) * exp(1j*phis(k)) ) / norm(h_opt);
    alphas_opt{end + 1} = w.'*h_true;
end

% Spherical wavefront LoS beamformer amplitude:
alpha_LoS = h_true'*Hk(:,1)/norm(Hk(:,1));  % only using k=1
PG_LoS_dB = 10*log10(abs(alpha_LoS).^2);
fprintf(['The PG using the LoS component only is ' num2str(PG_LoS_dB) 'dB.\n']);

h_sum = sum(Hk,2);        % superposition of all K SMC channels h_k
% Non-optimized weights:
w = conj(h_sum)./norm(h_sum);
w_non_opt = w;            % save for later analysis

% Non-optimized (complete) amplitude:
alpha_complete_non_opt = h_true.'*w;
alpha_complete_non_opt = alpha_complete_non_opt * exp(1j*CONSTANT_PLOTTING_PHASE_SHIFT);  % apply possible phase shift for plotting
PG_complete_dB = 10*log10(abs(alpha_complete_non_opt).^2);
fprintf(['The complete PG from all non-optimized components is ' num2str(PG_complete_dB) 'dB.\n']);


% iv) Compute optimized weights:
w = conj(h_opt)./norm(h_opt);   % phase-optimized weights

% v) Compute the optimized predicted amplitude:
alpha_complete = h_true.'*w;
PG_complete_dB = 10*log10(abs(alpha_complete).^2);
fprintf(['The complete PG from all jointly optimized components is ' num2str(PG_complete_dB) 'dB.\n']);

% vi) Compute the optimal measured amplitude; MRT performed on the measured (assumed true) channel:
w_meas = conj(h_true)/norm(h_true);
alpha_meas =  w_meas.' * h_true;
PG_meas_dB = 10*log10(abs(alpha_meas).^2);
fprintf(['The maximum PG achievable (given perfect CSI) is ' num2str(PG_meas_dB) 'dB.\n']);

% Compute the PG distribution across the grid:
y = zeros([13 13]);
for n=1:169
    h_n = R(iWPTfrequency,:,n);
    y(n) = h_n*w;     % received grid amplitudes given the computed weights
end

% Wave field interpolation for plotting:
xx = linspace(URA.xv(1), URA.xv(end), 13);
yy = linspace(URA.yv(1), URA.yv(end), 13);
xq = linspace(URA.xv(1), URA.xv(end), INTERP_RES);
yq = linspace(URA.yv(1), URA.yv(end), INTERP_RES);
Re_y = real(y);
Im_y = imag(y);
interpRe = interp2(xx,yy.', Re_y, xq, yq.', 'spline');
interpIm = interp2(xx,yy.', Im_y, xq, yq.', 'spline');
y_interp = interpRe + 1j*interpIm;

f4 = figure(4);   %  ---------- ---------- ---------- ---------- ----------
f4.Position = [0 0 500 500];
imagesc(xq, yq, 10*log10(abs(y_interp.').^2)); hold on;
set(gca,'YDir','normal'); set(gca,'XDir','normal');
plot(URA.xv(iWPTantenna), URA.yv(iWPTantenna), 'bo');
text(URA.xv(iWPTantenna)+1e-2, URA.yv(iWPTantenna)+1e-2,sprintf('%2.1fdB',PG_complete_dB),'FontSize',PLOT_FS);

view(90,90)
axis equal
colormap(cmap_interp);
hb = colorbar;
hb.FontSize = PLOT_FS;
hb.LineWidth = PLOT_LW;
colormap(cmap_interp);
xlabel('x in m'), ylabel('y in m')
ylabel(hb,'PG in dB')
set(gca,'FontSize',PLOT_FS,'LineWidth',PLOT_LW);
xlim([URA.xv(end) URA.xv(1)]); ylim([URA.yv(1) URA.yv(end)]);
hold off
caxis([cmax_WPT-30 cmax_WPT])

if SHOW_FIGURE_TITLES
  title(['Geometry-based BF: k in \{' sprintf('%d ',1:K) '\}']);
end
% ---------- ---------- ---------- ---------- ---------- ---------- -------

% Beam from the "right" wall: % ---------- ---------- ---------- ----------
for k=2:K
  if(RW{k}.p_global(2) > 7) % search for k corresponding to the "right" wall
    break;
  end
end

% Compute the PG distribution across the grid:
y2 = zeros([13 13]);
for n=1:169
    h_n = R(iWPTfrequency,:,n);
    w2 = conj(Hk(:,k))./norm(Hk(:,k));
    y2(n) = h_n*w2;     % received grid amplitudes given the computed weights
end

% Wave field interpolation for plotting:
Re_y = real(y2); Im_y = imag(y2);
interpRe = interp2(xx,yy.', Re_y, xq, yq.', 'spline');
interpIm = interp2(xx,yy.', Im_y, xq, yq.', 'spline');
y2_interp = interpRe + 1j*interpIm;

f6 = figure(6); f6.Position = [500 0 500 500]; %  ---------- ---------- ---
imagesc(xq, yq, 10*log10(abs(y2_interp.').^2)); hold on;
set(gca,'YDir','normal'); set(gca,'XDir','normal');
plot(URA.xv(iWPTantenna), URA.yv(iWPTantenna), 'bo');
text(URA.xv(iWPTantenna)+1e-2, URA.yv(iWPTantenna)+1e-2,... 
    sprintf('%2.1fdB',10*log10(abs(h_true.'*w2)^2)),'FontSize',PLOT_FS);

view(90,90)
axis equal
colormap(cmap_interp);
hb = colorbar;
hb.FontSize = PLOT_FS;
hb.LineWidth = PLOT_LW;
colormap(cmap_interp);
xlabel('x in m'), ylabel('y in m');
ylabel(hb,'PG in dB');
set(gca,'FontSize',PLOT_FS,'LineWidth',PLOT_LW);
xlim([URA.xv(end) URA.xv(1)]); ylim([URA.yv(1) URA.yv(end)]);
hold off
cmaxf6 = max(max(10*log10(abs(y2_interp.').^2)));
caxis([cmaxf6-30 cmaxf6]);

if SHOW_FIGURE_TITLES
  title(['Geometry-based BF: k =' sprintf('%d',k)]);
end
% ---------- ---------- ---------- ---------- ---------- ---------- -------


%% WPT: B. Optimization of Beam-phases
% Part II) computation phasors alpha_k in the complex plane
%
% Input:  individual optimized/non-optimized phasors
% Output: plots of the appended phasors showing the impact of the SMC beam
%         phase-optimization procedure

% 1) NON-OPTIMIZED SMC BEAMS: ---------------------------------------------
f7 = figure(7);   %  ---------- ---------- ---------- ---------- ----------
f7.Position = [500 0 500 500];
previous_tip = [0;0];   % initialize phasor tip of "previous" iteration

% PG <-- h_meas | w <-- h_model
% Plot final phasor which is the result of the optimization:
[Re_alpha_complete,Im_alpha_complete] = pol2cart(angle(alpha_complete_non_opt),abs(alpha_complete_non_opt)); 
phasor_complete = [Re_alpha_complete; Im_alpha_complete];
complete_tip = previous_tip + phasor_complete;
plot_data = [previous_tip, complete_tip];
plot(plot_data(1,:),plot_data(2,:),'r--','LineWidth',PLOT_LW);
hold on;

R_LIM = abs(alpha_meas)*1.05;     % plot limits
xlim([-R_LIM R_LIM]);  ylim([-R_LIM R_LIM]);

% PG <-- h_meas | w <-- h_meas
% Plot optimal phasor which is the result MRT applied at h_meas:
[Re_alpha_meas,Im_alpha_meas] = pol2cart(angle(alpha_meas),abs(alpha_meas)); 
phasor_meas = [Re_alpha_meas; Im_alpha_meas];
complete_tip = previous_tip + phasor_meas;
plot_data_m = [previous_tip, complete_tip];
offset = ones(size(plot_data)) * R_LIM/100;
plot(plot_data_m(1,:),plot_data_m(2,:)+offset,'b--','LineWidth',PLOT_LW);


% Plot the individual, non-optimized phasors that should lead (approx.) to the
% final result without optimization:
for k = 1:K
  alpha_k = alphas_non_opt{k}*exp(1j*CONSTANT_PLOTTING_PHASE_SHIFT); % no rotation
  [Re_alpha_k,Im_alpha_k] = pol2cart(angle(alpha_k), abs(alpha_k));
  phasor_k = [Re_alpha_k; Im_alpha_k];
  current_tip = previous_tip + phasor_k;
  plot_data = [previous_tip, current_tip];
  plot(plot_data(1,:),plot_data(2,:),'LineWidth',PLOT_LW);
  
  previous_tip = current_tip; % save last tip for next iteration
end
set(gca,'FontSize',PLOT_FS,'LineWidth',PLOT_LW);  pbaspect([1 1 1]);
xlabel('Re\{\alpha\}'); ylabel('Im\{\alpha\}');
hold off
if SHOW_FIGURE_TITLES
  title('Non-optimized phasors');
end
%  ---------- ---------- ---------- ---------- ---------- ---------- ------



% 2) PHASE-OPTIMIZED SMC BEAMS: -------------------------------------------
f8 = figure(8);   %  ---------- ---------- ---------- ---------- ----------
f8.Position = [1000 0 500 500];
previous_tip = [0;0];   % initialize phasor tip of "previous" iteration

% PG <-- h_meas | w <-- h_model
% Plot final phasor which is the result of the optimization:
[Re_alpha_complete,Im_alpha_complete] = pol2cart(0,abs(alpha_complete)); 
phasor_complete = [Re_alpha_complete; Im_alpha_complete];
complete_tip = previous_tip + phasor_complete;
plot_data = [previous_tip, complete_tip];
plot(plot_data(1,:),plot_data(2,:),'r--','LineWidth',PLOT_LW);
hold on;

xlim([-R_LIM R_LIM]);  ylim([-R_LIM R_LIM]);

% PG <-- h_meas | w <-- h_meas
% Plot optimal phasor which is the result MRT applied at h_meas:
plot(plot_data_m(1,:),plot_data_m(2,:)+offset,'b--','LineWidth',PLOT_LW);


% Plot the individual, optimized phasors that should lead (approx.) to the
% final optimization result:
for (k = 1:K)
  alpha_k = alphas_opt{k} * exp(-1j * angle(alpha_complete));       % rotate last tip to real axis
  [Re_alpha_k,Im_alpha_k] = pol2cart(angle(alpha_k), abs(alpha_k)); 
  phasor_k = [Re_alpha_k; Im_alpha_k];
  current_tip = previous_tip + phasor_k;
  plot_data = [previous_tip, current_tip];
  plot(plot_data(1,:),plot_data(2,:),'LineWidth',PLOT_LW);
  
  previous_tip = current_tip; % save last tip for next iteration
end
set(gca,'FontSize',PLOT_FS,'LineWidth',PLOT_LW);  pbaspect([1 1 1]);
xlabel('Re\{\alpha\}'); ylabel('Im\{\alpha\}');
hold off
if SHOW_FIGURE_TITLES
  title('Optimized phasors');
end
%  ---------- ---------- ---------- ---------- ---------- ---------- ------

fprintf('Done.\n');
fprintf('---------- ---------- ---------- ---------- ---------- ---------- ---------- \n');


%% Concluding remarks:
% Please run simulate_PG_distribution.m to simulate the path gain entirely
% and then plot it across the URA.
